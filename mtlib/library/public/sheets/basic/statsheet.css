/*
 * SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
 *
 * SPDX-License-Identifier: Aladdin
 */

/* The number of points deducted from the maximum.
   Because quantities can go negative it can be easier to think about how much has been lost
   and think about progress being taken away from the right hand side
   instead of the current amount stretching from the left. */
@property --damage {
  syntax: '<integer>';
  initial-value: 0;
  inherits: true;
}
/* The number of progress bars that should be displayed.
   Calculated by reexpressing the current amount as damage from the maximum,
   dividing by the amount per bar and rounding up.
   Theoretically this means at max this comes out to 0 but at least one is hard-coded.
   Also means when depleting the current bar it shows the next one due to an edge case,
   but this behaviour has the benefit of highlighting that going further negative is possible.*/
@property --bars {
  syntax: '<integer>';
  initial-value: 0;
  inherits: true;
}
/* Index identifying which negative threshold this bar is counting.
   e.g. from -10HP to 0HP is negative 1. */
@property --negative {
  syntax: '<integer>';
  initial-value: 0;
  inherits: false;
}

:root {
  --margin: 0.2em;
  --border-radius: 0.5em;
  color: var(--mt-theme-color-foreground);
}

body {
  position: fixed;
  padding: 0px;
  margin: 0px;
  background-color: rgba(0, 0, 0, 0);
}

/* NOTE: camelCase names are used instead of kebab-case because MapTool uses it for IDs
 * and handlebars variable names. */
#statSheet {
  padding: .5rem;
  border: .5rem solid;
  border-image: linear-gradient(to right bottom, var(--mt-theme-color-button-start-background), var(--mt-theme-color-button-end-background)) 1;
}

.portraitStatSummaryRow {
  display: flex;
  flex-direction: row;
  align-items: center;
}

.portrait {
}

.nameDetails {
  margin-bottom: 0px;
  padding-bottom: 0px;
}

.statDetails {
  padding-left: .5rem;
}

.playerProperty {
  color: var(--mt-theme-color-foreground);
  text-align: left;
}

.gmProperty {
  color: var(--mt-theme-color-objects-red-status);
  text-align: left;
}

.propertyValue {
  color: var(--mt-theme-color-foreground);
}

.propertyTable {
  margin: 0px;
  padding: 0px;
}

.tokenName {
  font-weight: bold;
  text-align: center;
}

.tokenGmName {
  font-weight: bold;
  color: var(--mt-theme-color-objects-red-status);
  text-align: center;
}


#pointBars {
  list-style-type: none;
  padding: 0;
}
.bar {
  --damage:  calc(var(--max) - var(--current));
  /* NOTE: WebKit doesn't round this down when storing into integer variables
   * and doesn't have the rounding function
   * so we instead calculate this in JavaScript. */
  /*--bars:  calc(clamp(1, (var(--damage)/var(--max)) + 0.5, 1 + var(--negatives)));*/
  /* Sets the baseline for future absolute positioned elements */
  position: relative;
  width: 100%;
}
/* The layer containing the point bars used to spread them out */
.thresholdLayer {
  /* Even column widths is awkward in flex since it ignored borders when content sizing.
     If we need to size something with a border try grid and figure out how to hide in that. */
  display: flex;
  justify-content: stretch;
  align-items: center;
  box-sizing: border-box;
  /* Position is absolute even though it's the back layer
     because one layer has to be relative to define the size of the container,
     the size should be based on the text,
     the text has to go at the front to be seen,
     and if this layer takes a relative position
     the subsequent relative layer will be relative to this
     making it more complicated to get it to the top of the bar element.
   */
  position: absolute;
  width: 100%;
  height: 100%;
}
/* The background layer for the unconditionally displayed point bar */
.threshold {
  /* We are using spans so that without CSS there's a renderable default,
     but spans default to inline display which flows inside a container
     but can't be resized. inline-block extends that to allow sizing.
   */
  display: inline-block;
  /* By default height applies to the contents of the element
     and then borders and margins get applied.
     Because this element has no contents and is being used to draw a shape
     we need to change the sizing to count as the size of the whole box
     so when we set the size it sets the size of the border.
   */
  box-sizing: border-box;
  background: black;
  border-radius: var(--border-radius);
  width: 100%;
  height: 100%;
}
/* The background layer for the conditionally displayed point bars */
.thresholdNegative {
  display: inline-block;
  box-sizing: border-box;
  background: black;
  border-radius: var(--border-radius);
  height: 100%;
  /* Width of each negative bar is calculated as subtracting the index from the number to display
     e.g. for 3 bars to display the first negative bar has index 1, so 3-1=2,
     the second negative is 3-2=1, the third is 3-3=0 and further are negative
     then constraining that between 0 and 1 and multiplying to 100%
     e.g. negative 1 clamps 2 to 1, negative 2 is already at 1, negative 3 is already 0
     and further negative bars get clamped to 0, and this becomes 100% or 0%.
     Flex will evenly shrink the 100% down to fit and leave the 0% undisplayed */
  width: calc(clamp(0, var(--bars) - var(--negative), 1)*100%);
  /*transition: width 2s;*/
}
.progress {
  --bars-damage: calc(var(--damage) / var(--max));
  --damage-progress: calc(var(--bars-damage)/var(--bars));
  display: inline-block;
  box-sizing: border-box;
  position: absolute;
  /* Each bar is logically divided up into a number of cells equal to --max.
     Because of margins the first and last cell are squashed.
     Because I want a single progress indicator stretching across all bars
     the cells joining bars are not */
  width: calc(100% * (1 - var(--damage-progress)));
  height: calc(100% - var(--border-radius) * 2);
  margin: var(--border-radius) 0;
  background: grey;
  /*transition: width 2s;*/
}
/* The progress class can be generic to other bars since variables are scoped.
   Other bars have different colours and need an ID for JS to find them
   which means we can conveniently use this ID to express the colours. */
#hpBar .progress, .bodyType .progress {
  background: red;
}
#fpBar .progress {
  background: green;
}
#mpBar .progress {
  background: blue;
}
#ipBar .progress {
  background: white;
}
#ipBar .statLabel {
  color: grey;
}
#ipBar .statValue {
  color: grey;
}

.statLabel {
  /* Allow resizing a span */
  display: inline-block;
  /* Containing elements by default size to their contents,
     but elements positioned absolutely aren't used to calculate this
     so we need a non-absolute element. */
  position: relative;
  color: white;
  width: 100%;
  margin-left: var(--margin);
}
/* The stat value is expressed as a layer with elements inside it
   because when replacing text through CSS that works by hiding the original,
   adding some new text afterwards and then repositioning the text over it.
   We don't have easy access to the position of the replaced text itself though
   if we use the trick of having a 100% width text box and center justifying
   so we need a layer that the value's text can float in the middle of.
 */
.statValueLayer {
  /* Define this layer as over the top of the label and backgrounds */
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  /* Position the contents in the middle using flex box layout
     since the usual trick of margin: auto to get it to fill the margins
     requires you to define the size of the contents. */
  display: flex;
  align-items: center;
  justify-content: center;
}
.statValue {
  /* Allow resizing a span */
  display: inline-block;
  /* Styling is inherited by the after pseudoclass */
  color: white;
  height: 100%;
  /* Hide the original value of the stats */
  visibility: hidden;
}
.statValue:after {
  /* Allow resizing a span, but also we are unconcerned by supporting adjacent elements */
  display: block;
  /* Reposition the after block over the top of the original */
  position: absolute;
  top: 0;
  /* If our text is shorter than the original
     centering it will get it close to the original position. */
  text-align: center;
  /* The visibility style of the class is inherited by the pseudoclass so we reset it. */
  visibility: initial;
  /* CSS is typed but has limited type coersion, so to turn the numbers into string
     we use a work-around where we turn them into counters first.
   */
  counter-reset: current var(--current) max var(--max);
  content: counter(current) "/" counter(max);
}

.locationDr {
  text-align: center;
}

.bodyType table, .bodyType thead, .bodyType tbody, .bodyType tr {
  display: contents;
}
.bodyType {
  display: grid;
  grid-template-columns: repeat(4, max-content) auto;
}
.bodyType tr:nth-of-type(1) th:nth-of-type(1) {
  grid-column: span 5;
}
.bodyType tr:nth-of-type(2) th:nth-of-type(2) {
  grid-column: span 2;
}
