[h: 'Make a dice roll with the provided modifiers']
[h: 'and return the result.']
[h: 'TODO: Currently also includes the original request']
[h: 'See crates/mt_gurps/src/lib.rs for js.rollAgainst']
[h: 'and mtlib/library/public/rollAgainst.html.in for caller.']
[h: 'TODO: Next time changes are required, rewrite this in JS.']
[h: vModifiers = json.get(macro.args, "modifiers")]
[h: vTarget = 0]
[h: vTargetDesc = ""]
[h,foreach(vModifier, vModifiers),code: {
  [vValue = json.get(vModifier, "value")]
  [vTarget = vTarget + vValue]
  [if(vTargetDesc == ""):
    vTargetDesc = vValue;
    vTargetDesc = strformat("%{vTargetDesc}%+d", vValue)]
}]
[h: vRoll = js.rollAgainst(vTarget)]
[h: broadcast(
  strformat("%s rolled against %s-%{vTarget} (%{vTargetDesc}) and got %s=%s with margin %s",
            player.getName(),
            json.get(json.get(vModifiers, 0), "name"),
            json.get(vRoll, "diceResult"),
            json.get(vRoll, "sum"),
            json.get(vRoll, "marginDescription"))
)]
[r: json.merge(macro.args, vRoll)]
