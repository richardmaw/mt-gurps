[h, if(argCount() == 2): vRes = js.rollABunchAgainst(arg(0), arg(1))]
[h, if(argCount() == 3): vRes = js.rollABunchAgainstWithCriticalSuccessMax(arg(0), arg(1), arg(2))]
Rolled  [r:json.get(vRes, "rollsSummary")]
[h: macro.return = vRes]
