[h: 'Calculate selected tokens grouped by their available hit locations']
[h: 'and send the result to the function in the web view requested.']
[h: 'See mtlib/library/public/purejsinit.js.in for the JS side,']
[h: 'and mtlib/library/public/injure.html.in for caller.']
[h: 'TODO: Next time changes are required, rewrite this in JS.']
[h: vInfo = "[]"]
[h,foreach(token, getSelected("json")),code: {
  [vLocations = js.getTokenLocationNames(token)]

  [vBucketIdx = -1]
  [for(i, 0, json.length(vInfo)),code: {
    [vILocs = json.get(json.get(vInfo, i), "locations")]
    [vEqual = json.equals(vLocations, vILocs)]
    [if(vEqual): vBucketIdx = i]
  }]

  [if(vBucketIdx == -1),code: {
    [vBucketIdx = json.length(vInfo)]
    [vTokens = json.set("{}", token, getTokenImage("", token))]
    [vBucket = json.set("{}", "locations", vLocations, "tokens", vTokens)]
    [vInfo = json.append(vInfo, vBucket)]
  }; {
    [vBucket = json.get(vInfo, vBucketIdx)]
    [vTokens = json.get(vBucket, "tokens")]
    [vTokens = json.set(vTokens, token, getTokenImage("", token))]
    [vBucket = json.set(vBucket, "tokens", vTokens)]
    [vInfo = json.set(vInfo, vBucketIdx, vBucket)]
  }]
}]
[h:macro.return = vInfo]
{vInfo}
