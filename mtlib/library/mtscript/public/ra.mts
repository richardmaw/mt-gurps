[h, if(argCount() == 1): vRes = js.rollAgainst(arg(0))]
[h, if(argCount() == 2): vRes = js.rollAgainstWithCriticalSuccessMax(arg(0), arg(1))]
Rolled [r:json.get(vRes, "diceResult")] result [r:json.get(vRes, "rollResult")] by [r:json.get(vRes, "margin")]
[h: macro.return = json.get(vRes, "marginDescription")]
