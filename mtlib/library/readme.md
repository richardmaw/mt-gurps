<!--
SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>

SPDX-License-Identifier: Aladdin
-->

### Macro functions

#### `ra(effective_skill[, extended_crit_range])`

#### `raba(rolls, effective_skill[, extended_crit_range])`

#### `injure(injury)`

#### `injureToken(tokenId, injury[, locationId])`

Apply injury to the token whose ID is `tokenId`,
updating the HP bar.

If `locationId` is provided it must be an array of strings
the first of which must match the `.id` field
of the `BodyType` property's `location` field
and subsequent IDs must recursively match locations inside that location's
`.subTable` field's `.locations` field.

If the location's `.locationId` is one of `eye`, `arm`, `leg`, `hand` or `foot`
then injury is applied to that location
and if it's not an `eye` then
any cumulative injury in excess of the location's HP is not applied to the
parent location or the token's total HP.

#### `showInjuryDialog()`

Display a dialog that reacts to which tokens are selected
and displays a different set of hit locations for different shaped enemies
so that you can apply the entered injury appropriately.

#### `fatigue(fatigue)`

#### `addSelectedToInitiative()`

Determine the initiative of selected tokens using their level of
Enhanced Time Sense (E), Basic Speed including Blinding Strike (B),
Dexterity (D) and a random number to avoid needing to roll (R)
resulting in a number of the form EBB.BBDDRR,
add it to initiative and a number of extra times based on the level of
Altered Time Rate
and sort Initiative in order of highest first.

#### `setInitiativeToSelected()`

As with `addSelectedToInitiative()` but without retaining the original list.

### Stat sheets

#### mt-gurps-basic

This displays the token, their name, the basic stats,
their HP, FP and Energy reserves, and their hit locations including limb injury.

### Events

#### `onInitiativeChange`

When initiative changes to a token that does not have Supernatural Durability
if they are on 0 or fewer HP or FP they will be prompted to roll HT or Will.

A dialog will be displayed for HT rolls.
When the roll option is submitted the result is logged to chat
and listed as an option in the dialog.
If a "Force Failure", "Critical Failure" or "Failure" option is submitted
then the dialog is closed and the token is marked as Unconscious.
