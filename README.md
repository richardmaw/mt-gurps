<!--
SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->
<!-- vim: set sw=2 et ts=2 sts=2 expandtab syntax=html : -->

# Unofficial GURPS MapTool Game Aid

[![Discord](
  https://img.shields.io/discord/296230822262865920?logo=discord&label=MapTool
)](https://discord.com/channels/296230822262865920/1302685383523303526)

This is an Unofficial GURPS Game Aid for MapTool
intended to automate book-keeping and provide reminders for forgettable rules
such as falling unconscious from injury or exhaustion, Major Wounds
and Accumulated Wounds to limbs.

## Usage

Add this add-on like any other, see Pages for documentation and instructions.

## Disclaimer

GURPS is a trademark of Steve Jackson Games,
and its rules and art are copyrighted by Steve Jackson Games.
All rights are reserved by Steve Jackson Games.
This game aid is the original creation of Richard Maw
and is released for free distribution, and not for resale,
under the permissions granted in the
[Steve Jackson Games Online Policy][].

## Build Instructions

To install dependencies:

```bash
apt install npm build-essential binaryen
rustup target add wasm32-unknown-unknown --toolchain stable
cargo install wasm-pack
```

To build the library:

```bash
make PROFILE=release
```

This should produce `com.gitlab.richardmaw.mt-gurps.mtlib` 
which you can then add to MapTool.

## Developer Notes

### Rebuilding the CI container

If the container needs new tools alter the `Dockerfile` and rebuild with:

```bash
docker login registry.gitlab.com   # Authenticate with Gitlab to allow pushing to container registry
docker build -t registry.gitlab.com/richardmaw/mt-gurps -f Dockerfile  # Create an image with a name that gitlab accepts
docker push registry.gitlab.com/richardmaw/mt-gurps  # Upload the new image
```

podman is a drop-in substitute if you don't want to taint your system.

### License

As a whole the project is non-commercially licensed
because unofficial GURPS game-aids cannot be sold.

Individual file licenses are annotated following <https://reuse.software>.

Source files and assets are licensed AFPL as a standard non-commercial license,
though the license's intent to sell license exceptions can't be exercised
since the implemented rules are copyright Steve Jackson Games
and used according to the [Steve Jackson Games Online Policy][].

Build files are licensed ISC so that they can be reused by other MapTool addons.

Documentation is licensed CC0.

[Steve Jackson Games Online Policy]: http://www.sjgames.com/general/online_policy.html
