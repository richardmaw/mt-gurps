# SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
#
# SPDX-License-Identifier: ISC

. + {
    name: $MTLIB_NAME,
    version: $VERSION,
    gitUrl: $FORGE_URL,
    namespace: $MTLIB_NAMESPACE,
} + if $ARGS.named | has("PAGES_URL") then {
    website: $PAGES_URL,
} else {
} end
