#!/usr/bin/python3

# SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
#
# SPDX-License-Identifier: ISC

import os
import pathlib
from shlex import quote
import subprocess
import sys
import tempfile
from urllib.parse import urlparse, urlunparse

if ("CI_REPOSITORY_URL" in os.environ and
    "CI_PAGES_URL" in os.environ and
    any(var in os.environ for var in ("CI_COMMIT_TAG", "CI_COMMIT_SHORT_SHA"))
   ):
    url = urlparse(os.environ["CI_REPOSITORY_URL"].replace("[masked]", ""))
    path = url.path.removesuffix(".git")
    netloc = url.hostname if url.port is None else f"{url.hostname}:{url.port}"
    forge_url = urlunparse(("https", netloc, path, "", "", ""))
    mtlib_name = path.split('/')[-1]
    mtlib_namespace = '.'.join(list(reversed(url.hostname.split('.'))) + path.split('/')[1:])
    pages_url = os.environ["CI_PAGES_URL"]
    version = os.environ.get("CI_COMMIT_TAG", os.environ["CI_COMMIT_SHORT_SHA"])
else:
    version = subprocess.run(["git", "describe", "--always", "--dirty"],
                             check=True, stdout=subprocess.PIPE, text=True).stdout.strip()
    url = subprocess.run(["git", "remote", "get-url", "origin"],
                         check=True, stdout=subprocess.PIPE, text=True).stdout.strip()
    if not any(url.startswith(prefix) for prefix in ("http", "ssh")):
        url = "ssh://" + url.replace(':', '/', 1)
    url = urlparse(url)
    path = url.path.removesuffix(".git")
    netloc = url.hostname if url.port is None else f"{url.hostname}:{url.port}"
    mtlib_name = path.split('/')[-1]
    mtlib_namespace = '.'.join(list(reversed(url.hostname.split('.'))) + path.split('/')[1:])
    forge_url = urlunparse(("https", netloc, path, "", "", ""))
    if url.hostname in ("github.com", "gitlab.com"):
        user = path.split('/')[1]
        pages_path = '/'.join(path.split('/')[2:])
        pages_netloc = f"{user}.{url.hostname.removesuffix('.com')}.io"
        pages_url = urlunparse(("https", pages_netloc, pages_path, "", "", ""))
    else:
        pages_url = None

output = pathlib.Path(sys.argv[1])

existing_vars = output.read_text() if output.exists() else ""

new_vars = f"""\
FORGE_URL={quote(forge_url)}
MTLIB_NAME={quote(mtlib_name)}
MTLIB_NAMESPACE={quote(mtlib_namespace)}
PAGES_URL={"" if pages_url is None else quote(pages_url)}
VERSION={quote(version)}
"""

if new_vars == existing_vars:
    exit()

with tempfile.TemporaryDirectory(prefix=f"._{output.name}", dir=output.parent, suffix=".tmp") as td:
    tf = pathlib.Path(td, output.name)
    tf.write_text(new_vars)
    os.rename(tf, output)
