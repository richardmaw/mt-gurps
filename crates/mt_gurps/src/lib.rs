// SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
//
// SPDX-License-Identifier: Aladdin

use core::fmt;
use std::fmt::Write;

use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = MTScript, js_name = registerMacro)]
    fn register_macro(macro_name: &str, callable: &js_sys::Function);
    #[wasm_bindgen(js_namespace = ["MapTool", "chat"])]
    fn broadcast(s: String);
}

#[derive(Debug, Deserialize, Serialize)]
enum RollResult {
    CriticalFailure,
    Failure,
    Success,
    CriticalSuccess,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct SkillRollReport {
    dice_result: [u8; 3],
    sum: u8,
    roll_result: RollResult,
    margin: i8,
}
struct SkillRollSummary<'a>(&'a SkillRollReport);
impl SkillRollReport {
    fn summary<'a>(&'a self) -> SkillRollSummary<'a> {
        SkillRollSummary(self)
    }
    fn into_roll_against_report(self) -> RollAgainstReport {
        let margin_description = format!("{}", self.summary());
        RollAgainstReport {
            skill_roll_report: self,
            margin_description,
        }
    }
}
impl<'a> fmt::Display for SkillRollSummary<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let margin = self.0.margin;
        write!(f, "{margin}")?;
        let description_suffix = match self.0.roll_result {
            RollResult::CriticalFailure | RollResult::CriticalSuccess => "!",
            _ => "",
        };
        write!(f, "{description_suffix}")
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct RollAgainstReport {
    #[serde(flatten)]
    skill_roll_report: SkillRollReport,
    margin_description: String,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct RollABunchAgainstReport {
    skill_rolls: Vec<SkillRollReport>,
    rolls_summary: String,
}
impl RollABunchAgainstReport {
    fn from_rolls(skill_rolls: Vec<SkillRollReport>) -> Self {
        let mut rolls_summary = String::new();
        for (i, roll) in skill_rolls.iter().enumerate() {
            let roll_summary = roll.summary();
            let res = match i {
                i if i == 0 => write!(&mut rolls_summary, "{}", roll_summary),
                i if i == skill_rolls.len() - 1 => {
                    write!(&mut rolls_summary, " and {}", roll_summary)
                }
                _ => write!(&mut rolls_summary, ", {}", roll_summary),
            };
            // Safety: formatted write to String is infallible
            unsafe { res.unwrap_unchecked() };
        }
        Self {
            skill_rolls,
            rolls_summary,
        }
    }
}

fn roll_skill(effective_skill: i8, crit_range: u8) -> SkillRollReport {
    // Safety: random returns an float in range [0, 1] so isn't NaN or infinite,
    // and when multiplied by 215 fits within a u8
    let rnd: u8 = unsafe { (js_sys::Math::random() * 215.0).to_int_unchecked() };
    let dice_result = [(rnd % 6) + 1, ((rnd % 36) / 6) + 1, (rnd / 36) + 1];
    let sum: u8 = dice_result.iter().sum();
    // Safety: roll can't be higher than 18
    let margin: i8 = effective_skill - unsafe { i8::try_from(sum).unwrap_unchecked() };
    let roll_result = match sum {
        0..=2 => unreachable!("Sum of 3 nonzero numbers can't be less than 3"),
        19.. => unreachable!("Sum of 3 numbers that are at most 6 can't exceed 18"),
        3 | 4 => RollResult::CriticalSuccess,
        i if margin >= 10 && i <= crit_range => RollResult::CriticalSuccess,
        17 if effective_skill >= 16 => RollResult::Failure,
        i if margin <= -10 => RollResult::CriticalFailure,
        17 => RollResult::CriticalFailure,
        18 => RollResult::CriticalFailure,
        i if margin >= 0 => RollResult::Success,
        _ => RollResult::Failure,
    };

    SkillRollReport {
        dice_result,
        sum,
        roll_result,
        margin,
    }
}

pub fn roll_against_with_critical_success_max_bg(
    effective_skill: i8,
    crit_range: u8,
) -> Result<JsValue, JsValue> {
    let res = roll_skill(effective_skill, crit_range).into_roll_against_report();
    Ok(serde_wasm_bindgen::to_value(&res)?)
}
pub fn roll_against_bg(effective_skill: i8) -> Result<JsValue, JsValue> {
    roll_against_with_critical_success_max_bg(effective_skill, 6)
}
pub fn roll_a_bunch_against_with_critical_success_max_bg(
    rolls: u8,
    effective_skill: i8,
    crit_range: u8,
) -> Result<JsValue, JsValue> {
    let skill_rolls: Vec<_> = (0..rolls)
        .into_iter()
        .map(|_| roll_skill(effective_skill, crit_range))
        .collect();
    let res = RollABunchAgainstReport::from_rolls(skill_rolls);
    Ok(serde_wasm_bindgen::to_value(&res)?)
}
pub fn roll_a_bunch_against_bg(rolls: u8, effective_skill: i8) -> Result<JsValue, JsValue> {
    roll_a_bunch_against_with_critical_success_max_bg(rolls, effective_skill, 6)
}

#[wasm_bindgen(start)]
fn run() -> Result<(), JsValue> {
    broadcast("GURPS loading".to_owned());
    // TODO: Generate this with macros
    let ra = Closure::<dyn Fn(i8) -> Result<JsValue, JsValue>>::new(roll_against_bg);
    register_macro("rollAgainst", ra.into_js_value().unchecked_ref());
    let rac = Closure::<dyn Fn(i8, u8) -> Result<JsValue, JsValue>>::new(
        roll_against_with_critical_success_max_bg,
    );
    register_macro(
        "rollAgainstWithCriticalSuccessMax",
        rac.into_js_value().unchecked_ref(),
    );
    let raba = Closure::<dyn Fn(u8, i8) -> Result<JsValue, JsValue>>::new(roll_a_bunch_against_bg);
    register_macro("rollABunchAgainst", raba.into_js_value().unchecked_ref());
    let rabac = Closure::<dyn Fn(u8, i8, u8) -> Result<JsValue, JsValue>>::new(
        roll_a_bunch_against_with_critical_success_max_bg,
    );
    register_macro(
        "rollABunchAgainstWithCriticalSuccessMax",
        rabac.into_js_value().unchecked_ref(),
    );

    Ok(())
}
