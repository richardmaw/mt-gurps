# SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
#
# SPDX-License-Identifier: ISC

RSLIB_MACRO_NAME = mt_gurps

CARGO_TARGET_DIR != cargo metadata --no-deps --format-version=1 | jq -r .target_directory

PROFILE ?= dev
PROFILE_DIR_dev = debug
PROFILE_DIR_$(PROFILE) ?= $(PROFILE)
WASM_PACK_OPTS_dev = --dev
WASM_PACK_OPTS_release = --release
PARCEL_OPTS_dev = --no-optimize
PARCEL_OPTS_release =

PROFILE_TARGET_DIR = $(CARGO_TARGET_DIR)/wasm32-unknown-unknown/$(PROFILE_DIR_$(PROFILE))

all: mtlib mtprops

FORCE:
.PHONY: all mtlib mtprops pages FORCE

vars: tools/genvars.py FORCE
	@echo Checking for changes to vars
	@python3 tools/genvars.py $@

include vars

mtlib: $(MTLIB_NAMESPACE)-$(VERSION).mtlib
mtprops: $(MTLIB_NAMESPACE)-$(VERSION).mtprops

# Build the Rust code
$(PROFILE_TARGET_DIR)/%.wasm:
	cargo build --package=$(@:$(PROFILE_TARGET_DIR)/%.wasm=%) --target=wasm32-unknown-unknown --profile=$(PROFILE)

%: %.in vars
	. ./vars && \
	sed -e s,@MTLIB_NAMESPACE@,"$${MTLIB_NAMESPACE}",g \
	    -e s,@MTLIB_NAME@,"$${MTLIB_NAME}",g $< >$@

# Add bindings to wasm
pkg/%.js pkg/%_bg.js pkg/%_bg.wasm: $(PROFILE_TARGET_DIR)/%.wasm
	package=$(<:$(PROFILE_TARGET_DIR)/%.wasm=%); \
	package_dir="$$(dirname \
		"$$(cargo metadata --no-deps --format-version=1 | \
			jq --arg n $$package -r '.packages[] | select(.name == $$n).manifest_path' \
		)" \
	)"; \
	wasm-pack build --no-package --no-typescript \
		 --out-dir "$$(readlink -f pkg)" $(WASM_PACK_OPTS_$(PROFILE)) \
		 "$${package_dir}"


# Convert wasm to asmjs-like JavaScript and adjust bindings
bundle/src/%_bg.wasm.js: pkg/%_bg.wasm
	wasm2js -o $@ $<
bundle/src/%_bg.js: pkg/%_bg.js vendor/FastestSmallestTextEncoderDecoder-master/EncoderDecoderTogether.src.js
	cat vendor/FastestSmallestTextEncoderDecoder-master/EncoderDecoderTogether.src.js $< | sed 's,_bg.wasm,&.js,g' >$@
bundle/src/%.js: pkg/%.js
	sed 's,_bg.wasm,&.js,g' $< >$@


# Transpile the scripts, generating a bundle that can be loaded in MapTool
bundle/%.zip: bundle/src/%.js
	cd bundle && \
	npx parcel build $(PARCEL_OPTS_$(PROFILE)) --dist-dir .dist.$$$$/library/js $(<:bundle/%=%) && \
	(cd .dist.$$$$ && zip -r - .) >../$@; \
	ret=$$?; \
	rm -rf .dist.$$$$; \
	exit $$?

-include $(PROFILE_TARGET_DIR)/$(RSLIB_MACRO_NAME).d

bundle/$(RSLIB_MACRO_NAME).zip: bundle/src/$(RSLIB_MACRO_NAME).js bundle/src/$(RSLIB_MACRO_NAME)_bg.js bundle/src/$(RSLIB_MACRO_NAME)_bg.wasm.js
ZIPS += bundle/$(RSLIB_MACRO_NAME).zip


mtlib/library.json: mtlib/library.json.in vars tools/addvars.jq
	. ./vars && \
	jq --arg MTLIB_NAME "$$MTLIB_NAME" \
		--arg MTLIB_NAMESPACE "$$MTLIB_NAMESPACE" \
		--arg VERSION "$$VERSION" \
		--arg FORGE_URL "$$FORGE_URL" \
		--arg PAGES_URL "$$PAGES_URL" \
		-f tools/addvars.jq $< \
		>$@

mtlib.zip.MANIFEST: | mtlib/library.json

mtlib/library/license.txt: LICENSE.txt
	cp $^ $@

mtlib.zip.MANIFEST: | mtlib/library/license.txt
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/fatigue.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/injure.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/injureToken.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/onInit.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/showInjureDialog.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/showRollAgainstDialog.mts
mtlib.zip.MANIFEST: | mtlib/library/mtscript/public/submitInjure.mts
mtlib.zip.MANIFEST: | mtlib/library/public/gcs.html
mtlib.zip.MANIFEST: | mtlib/library/public/injure.html
mtlib.zip.MANIFEST: | mtlib/library/public/purejsinit.js
mtlib.zip.MANIFEST: | mtlib/library/public/rollAgainst.html
mtlib.zip.MANIFEST: FORCE
	@echo Checking for changes in mtlib
	@tf=._$@.$$$$.tmp; (cd mtlib && find * -type f '(' '!' -name '.*' '!' -name '*.in' '!' -name '*.license' ')' -print) | sort -u >"$$tf" && \
	if test -e $@ && cmp "$$tf" $@ >/dev/null 2>&1; then \
		rm "$$tf"; \
	else \
		mv "$$tf" $@; \
	fi

mtlib.zip.d: mtlib.zip.MANIFEST
	@tf=._$@.$$$$.tmp; \
	sed 's,.*,$(@:.d=): mtlib/&,' <$< >"$$tf" && \
	mv "$$tf" $@

-include mtlib.zip.d

mtlib.zip: mtlib.zip.MANIFEST
	tf=._$@.$$$$.tmp; \
	(cd mtlib && zip -MM "../$$tf" -@) <$< && \
	mv "$$tf" $@

ZIPS += mtlib.zip

$(MTLIB_NAMESPACE)-$(VERSION).mtlib: $(ZIPS)
	mkdir .$@.tmp.$$$$ && \
	for z in $+; do \
		unzip $$z -d .$@.tmp.$$$$; \
	done && \
	(cd .$@.tmp.$$$$ && zip -r tmp.zip .) && \
	mv .$@.tmp.$$$$/tmp.zip $@; \
	ret=$$?; \
	rm -rf .$@.tmp.$$$$; \
	exit $$?

$(MTLIB_NAMESPACE)-$(VERSION).mtprops.MANIFEST: FORCE
	@echo Checking for changes in mtprops
	@tf=._$@.$$$$.tmp; (cd mtprops && find * -type f '(' '!' -name '.*' '!' -name '*.in' '!' -name '*.license' ')' -print) | sort -u >"$$tf" && \
	if test -e $@ && cmp "$$tf" $@ >/dev/null 2>&1; then \
		rm "$$tf"; \
	else \
		mv "$$tf" $@; \
	fi

$(MTLIB_NAMESPACE)-$(VERSION).mtprops.d: $(MTLIB_NAMESPACE)-$(VERSION).mtprops.MANIFEST
	@tf=._$@.$$$$.tmp; \
	sed 's,.*,$(@:.d=): mtprops/&,' <$< >"$$tf" && \
	mv "$$tf" $@

-include $(MTLIB_NAMESPACE)-$(VERSION).mtprops.d
$(MTLIB_NAMESPACE)-$(VERSION).mtprops: $(MTLIB_NAMESPACE)-$(VERSION).mtprops.MANIFEST
	(cd mtprops && zip -MM ../.$@.tmp.$$$$ -@ <../$<) && \
	mv .$@.tmp.$$$$ $@

pages: public/index.html

public/index.html: public/index.html.in vars mtlib/library/readme.md
	. ./vars && \
	cmark mtlib/library/readme.md | sed \
		-e "s,@FORGE_URL@,$${FORGE_URL},g" \
		-e "s,@MTLIB_NAME@,$${MTLIB_NAME},g" \
		-e '/@ADDON_README@/{' -e 'r /dev/stdin' -e d -e '}' \
		$< >$@
