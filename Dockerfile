# SPDX-FileCopyrightText: 2024 Richard Maw <richard.maw@gmail.com>
#
# SPDX-License-Identifier: ISC

FROM debian
RUN apt update && \
	apt -y install \
	build-essential \
	binaryen \
	cmark \
	curl \
	jq \
	make \
	npm \
	python3 \
	reuse \
	zip && \
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh /proc/self/fd/0 -y && \
	. "$HOME/.cargo/env" && \
	rustup target add wasm32-unknown-unknown --toolchain stable && \
	rustup toolchain install nightly && \
	cargo install wasm-pack
